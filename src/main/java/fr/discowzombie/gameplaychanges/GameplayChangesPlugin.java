package fr.discowzombie.gameplaychanges;

import fr.discowzombie.gameplaychanges.configuration.Configurations;
import fr.discowzombie.gameplaychanges.configuration.GeneralConfiguration;
import fr.discowzombie.gameplaychanges.listeners.*;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.logging.Level;

public final class GameplayChangesPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        // Config
        this.getDataFolder().mkdirs();

        GeneralConfiguration conf;
        try {
            conf = Configurations.copyAndLoadJson(
                    getResource("config.json"),
                    Paths.get(this.getDataFolder().getPath(), "config.json").toFile(),
                    GeneralConfiguration.class);
        } catch (IOException e) {
            this.getLogger().log(Level.SEVERE, "Unable to copy configuration file", e);
            Bukkit.getServer().shutdown();
            return;
        }

        final var pluginManager = Bukkit.getPluginManager();

        // Don't drop mob head
        if (conf.noHeadDrop)
            pluginManager.registerEvents(new MobDropHeadListener(), this);

        // Don't allow mob to spawn on nether roof
        if (conf.preventMobSpawningOnNetherRoof)
            pluginManager.registerEvents(new MobSpawningNetherRoof(), this);

        // Prevent villager and zombie villager spawning
        if (conf.preventVillagerAndZombieVillagerSpawning)
            pluginManager.registerEvents(new PreventVillagerSpawning(), this);

        // Transfer grindstone item enchantments to books
        if (conf.grindstoneBooks)
            pluginManager.registerEvents(new GrindstoneExtractEnchantments(this), this);

        // Prevent creating a bed bomb if players are in the radius
        if (conf.bedBombPlayerRadius)
            pluginManager.registerEvents(new PreventBedBombOtherPlayers(), this);

        // Prevent rabbit spawning
        if (conf.preventRabbitSpawning)
            pluginManager.registerEvents(new PreventRabbitSpawn(), this);

        // Nether reduction factor
        if (conf.netherCoordinateFactor != 8)
            pluginManager.registerEvents(new NetherReductionFactor(conf.netherCoordinateFactor), this);

        // Countdown between portal changes (to preserve from chunk load/unload). -1 or 0 to disable
        if (conf.countDownPortalTeleportSeconds > 0)
            pluginManager.registerEvents(new PortalChangeCountdown(conf.countDownPortalTeleportSeconds), this);

        if (conf.blockChunkLoader)
            pluginManager.registerEvents(new BlockChunkLoader(), this);

        if (conf.raidTimerSeconds > 0)
            pluginManager.registerEvents(new RaidTriggerTimerListener(this, conf.raidTimerSeconds), this);

        // Modify recipe results
        final var recipeIterator = Bukkit.recipeIterator();
        while (recipeIterator.hasNext()) {
            final var recipe = recipeIterator.next();

            // Don't allow to craft wood (6 face) variant
            if (conf.noWoodCraft) {
                final var typeName = recipe.getResult().getType().name();
                if (typeName.endsWith("_WOOD") && !typeName.startsWith("STRIPPED_")) {
                    recipeIterator.remove();
                    continue;
                }
            }

            // Don't allow cooking terracotta
            if (conf.noCookingTerracotta) {
                if (recipe.getResult().getType().name().endsWith("_GLAZED_TERRACOTTA")) {
                    recipeIterator.remove();
                    continue;
                }
            }
        }

        // Create the new membrane spawn
        if (conf.phantomMembraneCraft)
            Bukkit.addRecipe(
                    new ShapelessRecipe(
                            Objects.requireNonNull(NamespacedKey.fromString("phantom_membrane", this)),
                            new ItemStack(Material.PHANTOM_MEMBRANE)
                    ).addIngredient(3, new ItemStack(Material.STRING)));

        // Disable raids
        // Force enabled Raid if previously disabled
        Bukkit.getWorlds().forEach(w -> w.setGameRule(GameRule.DISABLE_RAIDS, conf.disableRaid));
    }
}
