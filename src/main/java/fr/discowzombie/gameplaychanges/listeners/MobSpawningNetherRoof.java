package fr.discowzombie.gameplaychanges.listeners;

import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public final class MobSpawningNetherRoof implements Listener {

    @EventHandler
    private void onMobSpawn(EntitySpawnEvent event) {
        final var location = event.getLocation();

        if (location.getWorld().getEnvironment() == World.Environment.NETHER
                && location.getBlockY() >= 128) {
            event.setCancelled(true);
        }
    }

}
