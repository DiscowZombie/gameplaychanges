package fr.discowzombie.gameplaychanges.listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEvent;

import java.util.List;

public final class BlockChunkLoader implements Listener {

    private static final List<EntityType> BLACKLISTED_ENTITIES = List.of(EntityType.BOAT, EntityType.CHEST_BOAT, EntityType.WARDEN);

    @EventHandler
    private void onEntityPortalEvent(EntityPortalEvent event) {
        if (BLACKLISTED_ENTITIES.contains(event.getEntityType()) ||
                event.getEntityType().name().startsWith("MINECART")) {
            event.setCancelled(true);
        }
    }
}
