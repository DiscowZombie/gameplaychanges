package fr.discowzombie.gameplaychanges.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.raid.RaidTriggerEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

public final class RaidTriggerTimerListener implements Listener {

    private static final String LAST_RAID_TRIGGERED_METADATA = "gpc.last_raid";

    private final JavaPlugin plugin;
    private final long raidDelayMs;

    public RaidTriggerTimerListener(final @NotNull JavaPlugin plugin, final int raidDelaySeconds) {
        this.plugin = plugin;
        this.raidDelayMs = raidDelaySeconds * 1000L;
    }

    @EventHandler
    private void onRaidTrigger(RaidTriggerEvent event) {
        final var player = event.getPlayer();

        final var metadataOpt = player.getMetadata(LAST_RAID_TRIGGERED_METADATA).stream()
                .filter(mv -> mv.getOwningPlugin() != null && mv.getOwningPlugin().equals(this.plugin))
                .findFirst();

        if (metadataOpt.isPresent() && (metadataOpt.get().asLong() + this.raidDelayMs) > System.currentTimeMillis()) {
            event.setCancelled(true);
            return;
        }

        // Write metadata
        player.setMetadata(LAST_RAID_TRIGGERED_METADATA, new FixedMetadataValue(this.plugin, System.currentTimeMillis()));
    }

}
