package fr.discowzombie.gameplaychanges.listeners;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.event.player.PlayerPortalEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class PortalChangeCountdown implements Listener {

    private final Map<UUID, Long> countdownPlayerPortalChange = new HashMap<>();
    private final Map<UUID, Long> countdownEntityPortalChange = new HashMap<>();

    private final int countDownPortalTeleportSeconds;

    public PortalChangeCountdown(int countDownPortalTeleportSeconds) {
        this.countDownPortalTeleportSeconds = countDownPortalTeleportSeconds;
    }

    @EventHandler
    private void onPlayerPortal(PlayerPortalEvent event) {
        final var player = event.getPlayer();

        final var lastChangeCd = this.countdownPlayerPortalChange.get(player.getUniqueId());

        if (lastChangeCd != null && (lastChangeCd + (this.countDownPortalTeleportSeconds * 1000L) > System.currentTimeMillis())) {
            player.sendActionBar(Component.text("Patientez quelques instants avant de reprendre un portail", NamedTextColor.RED));
            event.setCancelled(true);
        }
    }

    @EventHandler
    private void onEntityPortal(EntityPortalEvent event) {
        final var uniqueId = event.getEntity().getUniqueId();

        final var lastChangeCd = this.countdownEntityPortalChange.get(uniqueId);
        if (lastChangeCd != null && (lastChangeCd + (this.countDownPortalTeleportSeconds * 1000L) > System.currentTimeMillis())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onPlayerPassPortalEvent(PlayerPortalEvent event) {
        if (event.isCancelled()) // Don't update countdown
            return;
        final var player = event.getPlayer();
        this.countdownPlayerPortalChange.put(player.getUniqueId(), System.currentTimeMillis());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onEntityPassPortalEvent(EntityPortalEvent event) {
        if (event.isCancelled()) // Don't update countdown
            return;
        this.countdownEntityPortalChange.put(event.getEntity().getUniqueId(), System.currentTimeMillis());
    }
}
