package fr.discowzombie.gameplaychanges.listeners;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public final class PreventBedBombOtherPlayers implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        final var clickedBlock = event.getClickedBlock();
        if (clickedBlock == null)
            return;

        // Check for respawn anchor
        if (clickedBlock.getType() == Material.RESPAWN_ANCHOR &&
                !event.getPlayer().getWorld().isRespawnAnchorWorks()) {
            checkNearby(event, clickedBlock);
        }

        // Check for beds
        else if (clickedBlock.getType().name().endsWith("_BED") &&
                !event.getPlayer().getWorld().isBedWorks()) {
            checkNearby(event, clickedBlock);
        }
    }

    private void checkNearby(PlayerInteractEvent event, Block block) {
        // Check if more than one player on the radius
        if (block.getLocation().getNearbyPlayers(8).stream()
                .filter(p -> p.getGameMode() == GameMode.SURVIVAL || p.getGameMode() == GameMode.ADVENTURE)
                .count() > 1) {
            event.setCancelled(true);
        }
    }
}
