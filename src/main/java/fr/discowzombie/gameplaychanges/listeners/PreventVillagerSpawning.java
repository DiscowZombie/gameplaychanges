package fr.discowzombie.gameplaychanges.listeners;

import org.bukkit.entity.Villager;
import org.bukkit.entity.ZombieVillager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;

public final class PreventVillagerSpawning implements Listener {

    @EventHandler
    private void onEntitySpawn(EntitySpawnEvent event) {
        final var entity = event.getEntity();

        if (entity instanceof Villager || entity instanceof ZombieVillager) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    private void onTradeInventoryOpen(InventoryOpenEvent event) {
        if (event.getInventory().getType() == InventoryType.MERCHANT) {
            event.setCancelled(true);
        }
    }

}
