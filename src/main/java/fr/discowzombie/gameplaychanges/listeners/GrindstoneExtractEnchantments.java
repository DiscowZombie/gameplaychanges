package fr.discowzombie.gameplaychanges.listeners;

import com.destroystokyo.paper.event.inventory.PrepareResultEvent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.GrindstoneInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.plugin.java.JavaPlugin;

public final class GrindstoneExtractEnchantments implements Listener {

    private final JavaPlugin plugin;

    public GrindstoneExtractEnchantments(final JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    private void onGrindstoneEvent(PrepareResultEvent event) {
        if (!(event.getInventory() instanceof final GrindstoneInventory grindInventory))
            return;

        if (grindInventory.getUpperItem() == null ||
                grindInventory.getUpperItem().getEnchantments().isEmpty() ||
                grindInventory.getLowerItem() == null ||
                grindInventory.getLowerItem().getType() != Material.BOOK) {
            return;
        }

        // Construct the resulting item
        final var result = new ItemStack(Material.ENCHANTED_BOOK);
        final var itemMeta = (EnchantmentStorageMeta) result.getItemMeta();
        for (final var enchantmentEntry : grindInventory.getUpperItem().getEnchantments().entrySet()) {
            itemMeta.addStoredEnchant(enchantmentEntry.getKey(), enchantmentEntry.getValue(), false);
        }
        result.setItemMeta(itemMeta);

        Bukkit.getScheduler().runTask(this.plugin, () -> grindInventory.setResult(result));
    }

    @EventHandler
    private void onGrindstoneValidateEvent(InventoryClickEvent event) {
        if (!(event.getInventory() instanceof final GrindstoneInventory grindInventory))
            return;

        if (event.getSlotType() == InventoryType.SlotType.RESULT &&
                grindInventory.getUpperItem() != null &&
                !grindInventory.getUpperItem().getEnchantments().isEmpty() &&
                grindInventory.getLowerItem() != null &&
                grindInventory.getLowerItem().getType() == Material.BOOK) {
            final var upperItem = grindInventory.getUpperItem();
            final var lowerItem = grindInventory.getLowerItem();

            // Edit the original item
            final var meta = upperItem.getItemMeta();
            meta.getEnchants().keySet().forEach(meta::removeEnchant);
            upperItem.setItemMeta(meta);

            Bukkit.getScheduler().runTask(this.plugin, () -> {
                grindInventory.setUpperItem(upperItem);

                if (lowerItem.getAmount() > 1) {
                    // Set lower item amount
                    lowerItem.setAmount(lowerItem.getAmount() - 1);
                    grindInventory.setLowerItem(lowerItem);
                }
            });
        }
    }
}
