package fr.discowzombie.gameplaychanges.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public final class MobDropHeadListener implements Listener {

    @EventHandler
    private void onMobDeath(EntityDeathEvent event) {
        if (event.getEntity() instanceof Monster) {
            event.getDrops().removeIf(is -> is.getType() != Material.WITHER_SKELETON_SKULL && is.getType().name().endsWith("_SKULL"));
        }
    }
}
