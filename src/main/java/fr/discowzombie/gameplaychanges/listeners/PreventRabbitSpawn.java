package fr.discowzombie.gameplaychanges.listeners;

import org.bukkit.entity.Rabbit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public final class PreventRabbitSpawn implements Listener {

    @EventHandler
    private void onEntitySpawn(EntitySpawnEvent event) {
        if (event.getEntity() instanceof Rabbit) {
            event.setCancelled(true);
        }
    }
}
