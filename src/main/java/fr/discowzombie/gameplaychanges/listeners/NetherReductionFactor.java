package fr.discowzombie.gameplaychanges.listeners;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.event.player.PlayerPortalEvent;

public class NetherReductionFactor implements Listener {

    private final int netherCoordinateFactor;

    public NetherReductionFactor(int netherCoordinateFactor) {
        this.netherCoordinateFactor = netherCoordinateFactor;
    }

    @EventHandler
    private void onEntityPortalEvent(EntityPortalEvent event) {
        final var from = event.getFrom();
        final var fe = from.getWorld().getEnvironment();
        final var to = event.getTo();

        if (to == null)
            return;

        final var te = to.getWorld().getEnvironment();

        // Enter the nether
        if ((fe == World.Environment.NORMAL && te == World.Environment.NETHER)) {
            event.setTo(computeToLocation(from, to.getWorld(), 1d / this.netherCoordinateFactor));
        }

        // Exit the nether
        if ((fe == World.Environment.NETHER && te == World.Environment.NORMAL)) {
            event.setTo(computeToLocation(from, to.getWorld(), this.netherCoordinateFactor));
            event.setSearchRadius(64);
        }
    }

    @EventHandler
    private void onPlayerPortalEvent(PlayerPortalEvent event) {
        final var from = event.getFrom();
        final var fe = from.getWorld().getEnvironment();
        final var to = event.getTo();
        final var te = to.getWorld().getEnvironment();

        // Enter the nether
        if ((fe == World.Environment.NORMAL && te == World.Environment.NETHER)) {
            event.setTo(computeToLocation(from, to.getWorld(), 1d / this.netherCoordinateFactor));
        }

        // Exit the nether
        if ((fe == World.Environment.NETHER && te == World.Environment.NORMAL)) {
            event.setTo(computeToLocation(from, to.getWorld(), this.netherCoordinateFactor));
            event.setSearchRadius(64);
        }
    }

    private Location computeToLocation(final Location from, final World toWorld, final double factor) {
        return new Location(
                toWorld,
                (int) Math.floor(from.getX() * factor),
                (int) Math.floor(from.getY() * factor),
                (int) Math.floor(from.getZ() * factor));
    }
}
