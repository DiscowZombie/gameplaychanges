package fr.discowzombie.gameplaychanges.configuration;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public final class Configurations {

    public static final ObjectMapper MAPPER = createMapper();

    private Configurations() {
    }

    private static ObjectMapper createMapper() {
        final ObjectMapper mapper = new ObjectMapper();
        // General
        mapper.enable(JsonParser.Feature.ALLOW_COMMENTS); // Allow C/C++ style comments in JSON
        // Serialization
        mapper.enable(SerializationFeature.INDENT_OUTPUT); // Enable pretty-printing
        // Deserialization
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT); // Empty string "" will become null object (coercion)
        // Return-type
        return mapper;
    }

    public static <T> T loadJson(File file, Class<T> tClass) throws IOException {
        return MAPPER.readValue(file, tClass);
    }

    public static <T> T copyAndLoadJson(InputStream srcFile, File file, Class<T> tClass) throws IOException {
        copyIfNotExist(srcFile, file);
        return loadJson(file, tClass);
    }

    public static void copyIfNotExist(InputStream srcFile, File destFile) throws IOException {
        if (!destFile.exists()) {
            Files.copy(srcFile, destFile.toPath());
        }
    }
}
