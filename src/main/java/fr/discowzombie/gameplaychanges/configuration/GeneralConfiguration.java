package fr.discowzombie.gameplaychanges.configuration;

public class GeneralConfiguration {

    public boolean noHeadDrop;

    public boolean noWoodCraft;

    public boolean noCookingTerracotta;

    public boolean preventMobSpawningOnNetherRoof;

    public boolean preventVillagerAndZombieVillagerSpawning;

    public boolean grindstoneBooks;

    public boolean bedBombPlayerRadius;

    public boolean disableRaid;

    public boolean preventRabbitSpawning;

    public boolean phantomMembraneCraft;

    public int netherCoordinateFactor;

    public int countDownPortalTeleportSeconds;

    public boolean blockChunkLoader;

    public int raidTimerSeconds;
}
